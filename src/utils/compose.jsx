

function compose(...funcs) {
    return (comp) => {
        return funcs.reduceRight(
            (wrapped, f) => f(wrapped), comp
        )
    }
}
export{
    compose
} 