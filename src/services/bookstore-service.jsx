
export default class BookstoreService {

  data = [
    {
      id: 1,
      title: 'Prodyct-Ready Microservices',
      author: 'Susan J. Flover' ,
      price: 32,
      coverImage: 'https://images-na.ssl-images-amazon.com/images/I/41yJ75gpV-L._SX381_BO1,204,203,200_.jpg'
    },
    {
      id: 2,
      title: 'Relace It',
      author: 'Michal T' ,
      price: 50,
      coverImage: 'https://images-na.ssl-images-amazon.com/images/I/414CRjLjwgL._SX403_BO1,204,203,200_.jpg'
    }
  ] 

  getBooks() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
         resolve(this.data)
         reject(new Error('Error'))
        },1000)
    })
  }
}